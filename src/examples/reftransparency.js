"use strict";
exports.__esModule = true;
const Console_1 = require("fp-ts/lib/Console");
const IO_1 = require("fp-ts/lib/IO");
const pipeable_1 = require("fp-ts/lib/pipeable");
const fs = require("fs");
//
// funzioni di libreria
//
const readFile = function(filename) {
  return function() {
    return fs.readFileSync(filename, "utf-8");
  };
};
const writeFile = function(filename, data) {
  return function() {
    return fs.writeFileSync(filename, data, { encoding: "utf-8" });
  };
};
//
// programma
//
const read = pipeable_1.pipe(readFile("file.txt"), IO_1.chain(Console_1.log));
const program = pipeable_1.pipe(
  read,
  IO_1.chain(function() {
    return writeFile("./src/examples/file.txt", "hello");
  }),
  IO_1.chain(function() {
    return read;
  }),
);
program();
//
// programma 2
//
// function interleave<A, B>(a: IO<A>, b: IO<B>): IO<A> {
//   return pipe(
//     a,
//     chain(() => b),
//     chain(() => a)
//   );
// }
// const program2 = interleave(read, writeFile("./src/examples/file.txt", "foo"));

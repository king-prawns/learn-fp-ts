"use strict";
exports.__esModule = true;
const IO_1 = require("fp-ts/lib/IO");
const Date_1 = require("fp-ts/lib/Date");
const Console_1 = require("fp-ts/lib/Console");
const Random_1 = require("fp-ts/lib/Random");
const Monoid_1 = require("fp-ts/lib/Monoid");
const IO_2 = require("fp-ts/lib/IO");
const Array_1 = require("fp-ts/lib/Array");
const pipeable_1 = require("fp-ts/lib/pipeable");
const IO_3 = require("fp-ts/lib/IO");
function time(ma) {
  return IO_1.io.chain(Date_1.now, function(start) {
    return IO_1.io.chain(ma, function(a) {
      return IO_1.io.chain(Date_1.now, function(end) {
        return IO_1.io.map(Console_1.log("Elapsed: " + (end - start)), function() {
          return a;
        });
      });
    });
  });
}
exports.time = time;
function fib(n) {
  return n <= 1 ? 1 : fib(n - 1) + fib(n - 2);
}
const printFib = pipeable_1.pipe(
  Random_1.randomInt(30, 35),
  IO_3.chain(function(n) {
    return Console_1.log(fib(n));
  }),
);
function replicateIO(n, mv) {
  const result = Monoid_1.fold(IO_2.getMonoid(Monoid_1.monoidVoid))(Array_1.replicate(n, mv));
  return result;
}
// time(replicateIO(3, printFib))();
// Stampando anche i parziali
time(replicateIO(3, time(printFib)))();

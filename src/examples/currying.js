"use strict";
// g: (args: [B, C]) => D
const sum = function(args) {
  return args[0] + args[1];
};
const resultSum = sum([3, 4]);
console.log("resultSum", resultSum);
const sumCurrying = function(n) {
  return function(x) {
    return n + x;
  };
};
const resultCurrying = sumCurrying(3)(4);
console.log("resultCurrying", resultCurrying);

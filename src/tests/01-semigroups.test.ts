// https://github.com/gcanti/functional-programming#semigruppi

// DEFINIZIONE
// Sia A un insieme non vuoto e * un'operazione binaria chiusa su (o interna a) A
// ovvero *: A × A ⟶ A, allora la coppia (A, *) si chiama MAGMA.
// Sia (A, *) un magma, se * è associativa allora è un semigruppo.

// interface Magma<A> {
//   readonly concat: (x: A, y: A) => A;
// }
// interface Semigroup<A> extends Magma<A> {}

import { Predicate } from "fp-ts/lib/function";
import {
  fold,
  Semigroup,
  semigroupSum,
  semigroupString,
  semigroupProduct,
  getStructSemigroup,
  semigroupAll,
  getFunctionSemigroup,
} from "fp-ts/lib/Semigroup";

describe("a semigroup is an object with a concat function", () => {
  describe(`fold takes a semigroup as argument and returns a function 
    than concats many elements (given an initial value)`, () => {
    test("it sums an array of numbers", () => {
      const sum = fold(semigroupSum);
      expect(sum(0, [1, 2, 3, 4])).toBe(10);
    });

    test("it multiplies an array of numbers", () => {
      const product = fold(semigroupProduct);
      expect(product(1, [1, 2, 3, 4])).toBe(24);
    });

    test("it can be used to implement Array.every", () => {
      function every<A>(p: Predicate<A>, as: Array<A>): boolean {
        return fold(semigroupAll)(true, as.map(p));
      }
      function isLongerThan2(value: string): boolean {
        return value.length > 2;
      }
      expect(every(isLongerThan2, ["alcune", "stringhe", "lunghe"])).toBe(true);
      expect(every(isLongerThan2, ["un", "elemento", "corto"])).toBe(false);
    });

    test('it can be used to implement "Object.assign"', () => {
      const semigroupObject: Semigroup<object> = {
        concat: (x, y) => ({ ...x, ...y }),
      };
      function assign(as: Array<object>): object {
        return fold(semigroupObject)({}, as);
      }
      expect(assign([{ key1: "3" }, { key2: "5" }, { key2: "7" }])).toEqual({
        key1: "3",
        key2: "7",
      });
    });

    describe("a dual semigroup can be obtained inverting the arguments of concat", () => {
      function getDualSemigroup<A>(S: Semigroup<A>): Semigroup<A> {
        return {
          concat: (x, y): A => S.concat(y, x),
        };
      }

      test("it concats a string inverting the order", () => {
        expect(semigroupString.concat("1", "2")).toBe("12");
        const dualSemigroupString = getDualSemigroup(semigroupString);
        expect(dualSemigroupString.concat("1", "2")).toBe("21");
      });
    });

    describe("with getStructSemigroup we can combine semigroups to get a complex semigroup", () => {
      type Point = { x: number; y: number };
      type Vector = { from: Point; to: Point };

      const semigroupPoint: Semigroup<Point> = getStructSemigroup({
        x: semigroupSum,
        y: semigroupSum,
      });

      const semigroupVector: Semigroup<Vector> = getStructSemigroup({
        from: semigroupPoint,
        to: semigroupPoint,
      });

      expect(
        semigroupVector.concat(
          {
            from: { x: 5, y: 10 },
            to: { x: 100, y: 200 },
          },
          {
            from: { x: 505, y: 510 },
            to: { x: 1000, y: 2000 },
          },
        ),
      ).toEqual({
        from: { x: 510, y: 520 },
        to: { x: 1100, y: 2200 },
      });
    });

    describe("with getFunctionSemigroup we can get an instance of Semigroup<(a: never) => B> from a Semigroup<B>", () => {
      type Point = { x: number; y: number };

      /** `semigroupAll` is the boolean semigroup under conjunction */
      const semigroupPredicate = getFunctionSemigroup(semigroupAll)<Point>();

      const isPositiveX = (p: Point): boolean => p.x >= 0;
      const isPositiveY = (p: Point): boolean => p.y >= 0;

      const isPositiveXY = semigroupPredicate.concat(isPositiveX, isPositiveY);
      expect(isPositiveXY({ x: 1, y: 1 })).toBe(true);
      expect(isPositiveXY({ x: 1, y: -1 })).toBe(false);
      expect(isPositiveXY({ x: -1, y: 1 })).toBe(false);
      expect(isPositiveXY({ x: -1, y: -1 })).toBe(false);
    });
  });
});

// @flow
import { IO } from "fp-ts/lib/IO";
import { fold, Either, right, left } from "fp-ts/lib/Either";
import { pipe } from "fp-ts/lib/pipeable";
import { Task, map } from "fp-ts/lib/Task";

type ApiResult = { id: number; fetched: boolean };
type ApiCall = (id: number) => Promise<ApiResult>;
type Action =
  | { type: "SUCCEEDED"; payload: ApiResult }
  | { type: "ERROR"; payload: Error }
  | { type: "REQUESTED" };

describe("simulate a call to api and handle the result, the fp way", () => {
  function callFetchItem(id: number): Task<Either<Error, ApiResult>> {
    return (): Promise<Either<Error, ApiResult>> => Promise.resolve(right({ id, fetched: true }));
  }

  const handleApiResult = fold<Error, ApiResult, IO<Action>>(
    error => (): Action => ({
      payload: error,
      type: "ERROR",
    }),
    result => (): Action => ({
      payload: result,
      type: "SUCCEEDED",
    }),
  );

  function fetchItemAction(id: number): Task<IO<Action>> {
    return pipe(id, callFetchItem, map(handleApiResult));
  }

  test("it returns the api payload", async () => {
    const getPayload = await fetchItemAction(1)();
    expect(getPayload()).toEqual({
      payload: { id: 1, fetched: true },
      type: "SUCCEEDED",
    });
  });
});

describe("the previous example, more generic", () => {
  function callApi(api: ApiCall): (id: number) => Task<Either<Error, ApiResult>> {
    return function(id: number): Task<Either<Error, ApiResult>> {
      return async (): Promise<Either<Error, ApiResult>> => {
        try {
          return right(await api(id));
        } catch (error) {
          return left(error);
        }
      };
    };
  }

  const handleApiResult = fold<Error, ApiResult, IO<Action>>(
    error => (): Action => ({
      payload: error,
      type: "ERROR",
    }),
    result => (): Action => ({
      payload: result,
      type: "SUCCEEDED",
    }),
  );

  function thunkCreator(api: ApiCall): (id: number) => Task<IO<Action>> {
    return (id): Task<IO<Action>> => pipe(id, callApi(api), map(handleApiResult));
  }

  test("it returns the api payload", async () => {
    function fetchItem(id: number): Promise<ApiResult> {
      return Promise.resolve({ id, fetched: true });
    }
    const fetchItemAction = thunkCreator(fetchItem);

    const getPayload = await fetchItemAction(1)();

    expect(getPayload()).toEqual({
      payload: { id: 1, fetched: true },
      type: "SUCCEEDED",
    });
  });
});

describe("implement thunkCreator", () => {
  // type ThenArg<T> = T extends Array<Task<infer U>> ? U : T;
  // type InferAction<T extends (...args: any) => any> = ThenArg<ReturnType<ReturnType<T>>>;
  // type UserAction = InferAction<typeof getUserAction>;
  type Options = {
    dispatch: { request: boolean; error: boolean };
    namedParams: boolean;
    actionParams: Array<string>;
    responseName: string;
    catchError: boolean;
  };
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  type ApiCall2<T = never> = (...args: any[]) => Promise<T>;
  type Action2<T = never> =
    | { type: string; payload: T }
    | { type: string; payload: Error }
    | { type: string };
  type Dispatch<T = never> = (action: Action2<T>) => Action2<T>;

  function callApi<T = never>(api: ApiCall2<T>) {
    return function(...args: Parameters<ApiCall2<T>>): Task<Either<Error, T>> {
      return async (): Promise<Either<Error, T>> =>
        api(...args)
          .then(right)
          .catch(left);
    };
  }

  function handleApiResult<T>(type: string) {
    return (ma: Either<Error, T>): Action2<T> =>
      fold<Error, T, Action2<T>>(
        error => ({ payload: error, type: `${type}_ERROR` }),
        result => ({ payload: result, type: `${type}_SUCCEEDED` }),
      )(ma);
  }

  function startRequested(type: string): Task<Action2> {
    return (): Promise<Action2> => Promise.resolve({ type: `${type}_REQUESTED` });
  }

  function thunkCreator<T = never>(type: string, api: ApiCall2<T>) {
    return function asyncAction(...args: Parameters<ApiCall2<T>>) {
      return function(dispatch: Dispatch<T>): Promise<Action2<T>>[] {
        return [startRequested(type), pipe(callApi<T>(api)(...args), map(handleApiResult(type)))]
          .map(map(dispatch))
          .map(task => task());
      };
    };
  }

  type User = { id: number; name: string };
  const dispatch: Dispatch<User> = (action: Action2<User>): Action2<User> => action;

  test("it handles success", async () => {
    const getUser = (id: number): Promise<User> => Promise.resolve({ id, name: "Giorgio" });
    const getUserAction = thunkCreator<User>("USER", getUser);
    const results = await Promise.all(getUserAction(1)(dispatch));
    expect(results).toEqual([
      { type: "USER_REQUESTED" },
      { type: "USER_SUCCEEDED", payload: { id: 1, name: "Giorgio" } },
    ]);
  });

  test("it handles failure", async () => {
    const getUser = (): Promise<User> => Promise.reject(new Error("error!"));
    const getUserAction = thunkCreator<User>("USER", getUser);
    const results = await Promise.all(getUserAction()(dispatch));
    expect(results).toEqual([
      { type: "USER_REQUESTED" },
      { type: "USER_ERROR", payload: new Error("error!") },
    ]);
  });
});

// export default function thunkCreator(
//   type: string,
//   api: (...args?: any[]) => Promise<?mixed>,
//   options: $Shape<Options> = {},
// ): Function {
//   const { dispatch: dispatchOptions, actionParams: paramsNames, responseName, catchError } = {
//     ...defaultOptions,
//     ...options,
//   };

//   const requestType = `${type}_REQUESTED`;
//   const successType = `${type}_SUCCEEDED`;
//   const errorType = `${type}_FAILED`;

//   function asyncAction(...args: mixed[]) {
//     return function thunk(dispatch: Function) {
//       // This object will be destructured in success payload, giving meaninful
//       // property names to the argument values used in reducers
//       let argsMap = {};
//       if (options.namedParams && args.length === 1) {
//         [argsMap] = args;
//       } else {
//         argsMap = args.reduce((acc, argument, index) => {
//           acc[paramsNames[index]] = argument;
//           return acc;
//         }, {});
//       }

//       if (dispatchOptions.request) {
//         dispatch({ type: requestType, payload: argsMap });
//       }

//       return api(...args)
//         .then(apiResponse => {
//           const response = apiResponse;

//           dispatch({
//             payload: {
//               ...argsMap,
//               [responseName]: response,
//             },
//             type: successType,
//           });

//           return apiResponse;
//         })
//         .catch(error => {
//           if (axios.isCancel(error)) {
//             return;
//           }
//           if (!catchError) {
//             throw error;
//           }
//           dispatch({
//             error: dispatchOptions.error,
//             payload: {
//               ...argsMap,
//               error,
//             },
//             type: errorType,
//           });
//         });
//     };
//   }

//   return asyncAction;
// }

import { Show } from "fp-ts/lib/Show";
import { Monoid, getFunctionMonoid, monoidAll, monoidAny, fold } from "fp-ts/lib/Monoid";

describe("example: implement a program to draw shapes on console", () => {
  // a shape in space S is a function that given a point of S returns `true`
  // if the point belongs to the shape, `false` otherwise
  type Shape<S> = (point: S) => boolean;

  interface Point2D {
    x: number;
    y: number;
  }

  type Shape2D = Shape<Point2D>;

  function outside<S>(s: Shape<S>): Shape<S> {
    return (point): boolean => !s(point);
  }

  function distance(p1: Point2D, p2: Point2D): number {
    return Math.sqrt(Math.pow(Math.abs(p1.x - p2.x), 2) + Math.pow(Math.abs(p1.y - p2.y), 2));
  }

  function disk(center: Point2D, radius: number): Shape2D {
    return (point): boolean => distance(point, center) <= radius;
  }

  const showShape2D: Show<Shape2D> = {
    show: s => {
      let r = "───────────────────────\n";
      for (let j = 10; j >= -10; j--) {
        r += "│";
        for (let i = -10; i <= 10; i++) {
          r += s({ x: i, y: j }) ? "▧" : " ";
        }
        r += "│\n";
      }
      r += "───────────────────────";
      return r;
    },
  };

  describe("draw simple shapes", () => {
    test("it draws a disk", () => {
      const diskShape = disk({ x: 0, y: 0 }, 5);
      expect(showShape2D.show(diskShape)).toBe(
        `───────────────────────
│                     │
│                     │
│                     │
│                     │
│                     │
│          ▧          │
│       ▧▧▧▧▧▧▧       │
│      ▧▧▧▧▧▧▧▧▧      │
│      ▧▧▧▧▧▧▧▧▧      │
│      ▧▧▧▧▧▧▧▧▧      │
│     ▧▧▧▧▧▧▧▧▧▧▧     │
│      ▧▧▧▧▧▧▧▧▧      │
│      ▧▧▧▧▧▧▧▧▧      │
│      ▧▧▧▧▧▧▧▧▧      │
│       ▧▧▧▧▧▧▧       │
│          ▧          │
│                     │
│                     │
│                     │
│                     │
│                     │
───────────────────────`,
      );
    });

    test("it draws the complementary shape of a disk", () => {
      const complementaryShape = outside(disk({ x: 0, y: 0 }, 5));
      expect(showShape2D.show(complementaryShape)).toBe(
        `───────────────────────
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧ ▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧       ▧▧▧▧▧▧▧│
│▧▧▧▧▧▧         ▧▧▧▧▧▧│
│▧▧▧▧▧▧         ▧▧▧▧▧▧│
│▧▧▧▧▧▧         ▧▧▧▧▧▧│
│▧▧▧▧▧           ▧▧▧▧▧│
│▧▧▧▧▧▧         ▧▧▧▧▧▧│
│▧▧▧▧▧▧         ▧▧▧▧▧▧│
│▧▧▧▧▧▧         ▧▧▧▧▧▧│
│▧▧▧▧▧▧▧       ▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧ ▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
│▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧│
───────────────────────`,
      );
    });
  });

  describe("draw an intersection between shapes", () => {
    // we want to combine two shapes. Remember: a shape is a function Point => boolean;
    // if we have a monoid on boolean we can get a monoid on Point => boolean.
    // The intersection can be obtained using a monoid that combines two shapes
    const intersectMonoid: Monoid<Shape2D> = getFunctionMonoid(monoidAll)();
    function ring(point: Point2D, bigRadius: number, smallRadius: number): Shape2D {
      return intersectMonoid.concat(disk(point, bigRadius), outside(disk(point, smallRadius)));
    }

    test("it draws a ring", () => {
      const ringShape = ring({ x: 0, y: 0 }, 5, 3);
      expect(showShape2D.show(ringShape)).toBe(
        `───────────────────────
│                     │
│                     │
│                     │
│                     │
│                     │
│          ▧          │
│       ▧▧▧▧▧▧▧       │
│      ▧▧▧▧ ▧▧▧▧      │
│      ▧▧     ▧▧      │
│      ▧▧     ▧▧      │
│     ▧▧       ▧▧     │
│      ▧▧     ▧▧      │
│      ▧▧     ▧▧      │
│      ▧▧▧▧ ▧▧▧▧      │
│       ▧▧▧▧▧▧▧       │
│          ▧          │
│                     │
│                     │
│                     │
│                     │
│                     │
───────────────────────`,
      );
    });
  });

  describe("draws the union of shapes", () => {
    const unionMonoid: Monoid<Shape2D> = getFunctionMonoid(monoidAny)();

    function mickeyMouse(point: Point2D, faceRadius: number, earRadius: number): Shape<Point2D> {
      const leftEar = disk({ x: faceRadius * -1, y: faceRadius }, earRadius);
      const rightEar = disk({ x: faceRadius, y: faceRadius }, earRadius);
      const face = disk(point, faceRadius);
      return fold(unionMonoid)([leftEar, rightEar, face]);
    }

    test("it draws Mickey Mouse", () => {
      const mickeyMouseShape = mickeyMouse({ x: 0, y: 0 }, 5, 3);
      expect(showShape2D.show(mickeyMouseShape)).toBe(
        `───────────────────────
│                     │
│                     │
│     ▧         ▧     │
│   ▧▧▧▧▧     ▧▧▧▧▧   │
│   ▧▧▧▧▧     ▧▧▧▧▧   │
│  ▧▧▧▧▧▧▧ ▧ ▧▧▧▧▧▧▧  │
│   ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧   │
│   ▧▧▧▧▧▧▧▧▧▧▧▧▧▧▧   │
│     ▧▧▧▧▧▧▧▧▧▧▧     │
│      ▧▧▧▧▧▧▧▧▧      │
│     ▧▧▧▧▧▧▧▧▧▧▧     │
│      ▧▧▧▧▧▧▧▧▧      │
│      ▧▧▧▧▧▧▧▧▧      │
│      ▧▧▧▧▧▧▧▧▧      │
│       ▧▧▧▧▧▧▧       │
│          ▧          │
│                     │
│                     │
│                     │
│                     │
│                     │
───────────────────────`,
      );
    });
  });
});

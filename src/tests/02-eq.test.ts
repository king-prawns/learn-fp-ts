// https://github.com/gcanti/functional-programming#eq
// interface Eq<A> {
//   readonly equals: (x: A, y: A) => boolean;
// }
// These laws must be true:
// Reflexivity: equals(x, x) === true, for every x in A
// Symmetry: equals(x, y) === equals(y, x), for every x, y in A
// Transitivity: if equals(x, y) === true and equals(y, z) === true,
// then equals(x, z) === true, for every x, y, z in A

import { Eq, eqNumber, getStructEq, eqString, contramap } from "fp-ts/lib/Eq";
import { pipe } from "fp-ts/lib/pipeable";

describe("given an equality we can simply create a generic contains function", () => {
  function contains<A>(E: Eq<A>): (a: A, as: Array<A>) => boolean {
    return (a, as): boolean => as.some(item => E.equals(item, a));
  }

  describe("use eqNumber to search in an array of numbers", () => {
    const containsNumber = contains(eqNumber);
    test("it returns true if it finds the number", () => {
      expect(containsNumber(1, [1, 2, 3])).toBe(true);
    });

    test("it returns false otherwise", () => {
      expect(containsNumber(4, [1, 2, 3])).toBe(false);
    });
  });

  describe("define a new eq to search in an array of complex objects", () => {
    type Address = {
      street: string;
      city: string;
    };
    type User = {
      id: string;
      name: string;
      age: number;
      address: Address;
    };
    const arrayOfUsers = [
      {
        id: "656252",
        name: "Giovanni",
        age: 54,
        address: {
          street: "vittorio emanuele 12",
          city: "Milano",
        },
      },
      {
        id: "235266",
        name: "Stefano",
        age: 33,
        address: {
          street: "Santa Margherita 44",
          city: "Bologna",
        },
      },
      {
        id: "1234567",
        name: "Marco",
        age: 22,
        address: {
          street: "Via del gelso 4",
          city: "Lodi",
        },
      },
    ];
    const arrayOfAddresses = [
      { street: "Via del gelso 4", city: "Lodi" },
      { street: "vittorio emanuele 12", city: "Milano" },
      { street: "Santa Margherita 44", city: "Bologna" },
    ];

    describe("with contramap we can reduce an object to a number", () => {
      describe("two users are equal if their `id` field are equal", () => {
        const eqUser = pipe(
          eqString,
          contramap((user: User) => user.id),
        );
        const containsUser = contains(eqUser);

        it("should returns true if it finds an element", () => {
          const user = {
            id: "1234567",
            name: "Marco",
            age: 22,
            address: { street: "Via del gelso 4", city: "Lodi" },
          };
          expect(containsUser(user, arrayOfUsers)).toBe(true);
        });

        it("should returns false otherwise", () => {
          const user = {
            id: "333333",
            name: "Marco",
            age: 55,
            address: { street: "Via del gelso 4", city: "Bologna" },
          };
          expect(containsUser(user, arrayOfUsers)).toBe(false);
        });
      });
    });

    describe("with getStructEq we can combine eq to compare complex objects", () => {
      describe("two addresses are equals if all properties are equals", () => {
        const eqAddress: Eq<Address> = getStructEq({
          street: eqString,
          city: eqString,
        });
        const containsAddress = contains(eqAddress);

        it(" should returns true if it finds an element", () => {
          const address = { street: "Via del gelso 4", city: "Lodi" };
          expect(containsAddress(address, arrayOfAddresses)).toBe(true);
        });

        it("should returns false otherwise", () => {
          const address = { street: "Via del gelso 4", city: "Milano" };
          expect(containsAddress(address, arrayOfAddresses)).toBe(false);
        });
      });

      describe("two user are equals if all properties are equals", () => {
        const eqAddress: Eq<Address> = getStructEq({
          street: eqString,
          city: eqString,
        });
        const eqUser: Eq<User> = getStructEq({
          id: eqString,
          name: eqString,
          age: eqNumber,
          address: eqAddress,
        });
        const containsUser = contains(eqUser);

        it(" should returns true if it finds an element", () => {
          const user = {
            id: "1234567",
            name: "Marco",
            age: 22,
            address: { street: "Via del gelso 4", city: "Lodi" },
          };
          expect(containsUser(user, arrayOfUsers)).toBe(true);
        });

        it("should returns false otherwise", () => {
          const user = {
            id: "1234567",
            name: "Marco",
            age: 55,
            address: { street: "Via del gelso 4", city: "Bologna" },
          };
          expect(containsUser(user, arrayOfUsers)).toBe(false);
        });
      });
    });
  });
});

import {
  getStructSemigroup,
  getJoinSemigroup,
  getMeetSemigroup,
  Semigroup,
  semigroupAny,
  fold,
} from "fp-ts/lib/Semigroup";
import { pipe } from "fp-ts/lib/pipeable";
import {
  contramap,
  ordNumber,
  ordString,
  ordBoolean,
  Ord,
  fromCompare,
  getDualOrd,
} from "fp-ts/lib/Ord";
import { sort } from "fp-ts/lib/Array";

describe("implement a semigroup to concat customer's info", () => {
  interface Customer {
    name: string;
    favouriteThings: Array<string>;
    registeredAt: number; // since epoch
    lastUpdatedAt: number; // since epoch
    hasMadePurchase: boolean;
  }

  const semigroupCustomer: Semigroup<Customer> = getStructSemigroup({
    name: getJoinSemigroup(
      pipe(
        ordNumber,
        contramap<number, string>(name => name.length),
      ),
    ),
    favouriteThings: { concat: (a: string[], b: string[]): string[] => a.concat(b) },
    registeredAt: getMeetSemigroup(ordNumber),
    lastUpdatedAt: getJoinSemigroup(ordNumber),
    hasMadePurchase: semigroupAny,
  });

  test("it merges the contacts", () => {
    expect(
      semigroupCustomer.concat(
        {
          name: "Giulio",
          favouriteThings: ["math", "climbing"],
          registeredAt: new Date(2018, 1, 20).getTime(),
          lastUpdatedAt: new Date(2018, 2, 18).getTime(),
          hasMadePurchase: false,
        },
        {
          name: "Giulio Canti",
          favouriteThings: ["functional programming"],
          registeredAt: new Date(2018, 1, 22).getTime(),
          lastUpdatedAt: new Date(2018, 2, 9).getTime(),
          hasMadePurchase: true,
        },
      ),
    ).toEqual({
      name: "Giulio Canti", // the longer name
      favouriteThings: ["math", "climbing", "functional programming"], // concat things
      registeredAt: 1519081200000, // least recent date
      lastUpdatedAt: 1521327600000, // most recent date
      hasMadePurchase: true, // boolean disjunction
    });
  });
});

describe("given a type A we can get an instance of Semigroup<Ord<A>>", () => {
  // this means we can combine ords on A

  function getSemigroup(): Semigroup<Ord<User>> {
    return {
      concat: (x, y): Ord<User> =>
        fromCompare((a, b) => (x.compare(a, b) !== 0 ? x.compare(a, b) : y.compare(a, b))),
    };
  }

  const semigroupUserOrders = getSemigroup();

  interface User {
    id: number;
    name: string;
    age: number;
    rememberMe: boolean;
  }

  const byName = pipe(
    ordString,
    contramap((p: User) => p.name),
  );
  const byAge = pipe(
    ordNumber,
    contramap((p: User) => p.age),
  );
  const byRememberMe = pipe(
    ordBoolean,
    contramap((p: User) => p.rememberMe),
  );

  const users: Array<User> = [
    { id: 1, name: "Guido", age: 47, rememberMe: false },
    { id: 2, name: "Guido", age: 46, rememberMe: true },
    { id: 3, name: "Giulio", age: 44, rememberMe: false },
    { id: 4, name: "Giulio", age: 44, rememberMe: true },
  ];

  describe("sort an array of users by name, then by age and then by 'rememberMe' = false", () => {
    const order = fold(semigroupUserOrders)(byName, [byAge, byRememberMe]);

    test("it sorts the array", () => {
      expect(sort(order)(users)).toEqual([
        { id: 3, name: "Giulio", age: 44, rememberMe: false },
        { id: 4, name: "Giulio", age: 44, rememberMe: true },
        { id: 2, name: "Guido", age: 46, rememberMe: true },
        { id: 1, name: "Guido", age: 47, rememberMe: false },
      ]);
    });
  });

  describe("sort an array of users by rememberMe = true, then by desc name and then by age", () => {
    const order = fold(semigroupUserOrders)(getDualOrd(byRememberMe), [getDualOrd(byName), byAge]);

    test("it sorts the array", () => {
      expect(sort(order)(users)).toEqual([
        { id: 2, name: "Guido", age: 46, rememberMe: true },
        { id: 4, name: "Giulio", age: 44, rememberMe: true },
        { id: 1, name: "Guido", age: 47, rememberMe: false },
        { id: 3, name: "Giulio", age: 44, rememberMe: false },
      ]);
    });
  });
});

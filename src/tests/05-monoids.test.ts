import { Monoid, fold, getStructMonoid, monoidSum } from "fp-ts/lib/Monoid";

// https://github.com/gcanti/functional-programming#monoidi

// interface Monoid<A> extends Semigroup<A> {
//   readonly empty: A
// }
// These laws must be true:
// Right identity: concat(a, empty) = a, for every a in A
// Left identity: concat(empty, a) = a, for every a in A

describe("every endomorphism has a monoid instance", () => {
  type Endomorphism<A> = (a: A) => A;

  function identity<A>(a: A): A {
    return a;
  }

  function getEndomorphismMonoid<A = never>(): Monoid<Endomorphism<A>> {
    return {
      concat: (x, y): Endomorphism<A> => (a): A => x(y(a)),
      empty: identity,
    };
  }

  describe("example: get the monoid of an endorphism on string ", () => {
    const toUpperCase = (value: string): string => value.toUpperCase();
    const removeVowels = (value: string): string => value.replace(/[aeiou]/, "");
    const monoid = getEndomorphismMonoid<string>();

    test("concat combines two functions", () => {
      expect(monoid.concat(toUpperCase, removeVowels)("mystring")).toBe("MYSTRNG");
    });

    test("right identity works", () => {
      expect(monoid.concat(toUpperCase, monoid.empty)("mystring")).toBe("MYSTRING");
    });

    test("left identity works", () => {
      expect(monoid.concat(monoid.empty, toUpperCase)("mystring")).toBe("MYSTRING");
    });
  });

  describe("if M has a monoid instance then (a: A) => M has a monoid instance", () => {
    function getFunctionMonoid<M>(M: Monoid<M>): <A = never>() => Monoid<(a: A) => M> {
      return <A = never>(): Monoid<(a: A) => M> => ({
        concat: (f, g) => (a): M => M.concat(f(a), g(a)),
        empty: (): M => M.empty,
      });
    }

    describe("example: get an instance of Monoid<string => boolean>", () => {
      /** boolean monoid under disjunction */
      const monoidAny: Monoid<boolean> = {
        concat: (x, y) => x || y,
        empty: false,
      };
      const containsA = (s: string): boolean => s.indexOf("a") !== -1;
      const containsB = (s: string): boolean => s.indexOf("b") !== -1;
      const functionMonoid = getFunctionMonoid(monoidAny)<string>();
      const containsAOrB = functionMonoid.concat(containsA, containsB);

      test("it checks if a string contains 'a' or 'b'", () => {
        expect(containsAOrB("value")).toBe(true);
        expect(containsAOrB("nope")).toBe(false);
      });
    });

    describe("reducers have a monoid instance", () => {
      type Reducer<S, A> = (a: A) => (s: S) => S;

      function getReducerMonoid<S, A>(): Monoid<Reducer<S, A>> {
        return getFunctionMonoid(getEndomorphismMonoid<S>())<A>();
      }

      describe("example: combines two reducers that operate on the same state", () => {
        type State = { count: number; nActions: number };
        type Action = { type: string };
        function incrementReducer(action: Action): (state: State) => State {
          return (s: State): State => {
            if (action.type === "increment") {
              return { count: s.count + 1, nActions: s.nActions };
            }
            return s;
          };
        }
        function decrementReducer(action: Action): (state: State) => State {
          return (s: State): State => {
            if (action.type === "decrement") {
              return { count: s.count - 1, nActions: s.nActions };
            }
            return s;
          };
        }
        function countActionsReducer(): (state: State) => State {
          return (s: State): State => ({ count: s.count, nActions: s.nActions + 1 });
        }

        const combinedReducer = fold(getReducerMonoid<State, Action>())([
          incrementReducer,
          decrementReducer,
          countActionsReducer,
        ]);

        test("combinedReducer handles 'increment' action", () => {
          expect(combinedReducer({ type: "increment" })({ count: 1, nActions: 0 })).toEqual({
            count: 2,
            nActions: 1,
          });
        });

        test("combinedReducer handles 'decrement' action", () => {
          expect(combinedReducer({ type: "decrement" })({ count: 1, nActions: 0 })).toEqual({
            count: 0,
            nActions: 1,
          });
        });
      });
    });
  });
});

describe("with getStructMonoid we can combine monoids to get a complex monoid", () => {
  type Point = { x: number; y: number };
  type Vector = { from: Point; to: Point };

  const monoidPoint: Monoid<Point> = getStructMonoid({
    x: monoidSum,
    y: monoidSum,
  });

  const monoidVector: Monoid<Vector> = getStructMonoid({
    from: monoidPoint,
    to: monoidPoint,
  });

  expect(
    monoidVector.concat(
      {
        from: { x: 5, y: 10 },
        to: { x: 100, y: 200 },
      },
      {
        from: { x: 505, y: 510 },
        to: { x: 1000, y: 2000 },
      },
    ),
  ).toEqual({
    from: { x: 510, y: 520 },
    to: { x: 1100, y: 2200 },
  });
});

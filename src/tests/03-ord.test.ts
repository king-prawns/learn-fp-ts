// https://github.com/gcanti/functional-programming#ord

// type Ordering = -1 | 0 | 1
// interface Ord<A> extends Eq<A> {
//   readonly compare: (x: A, y: A) => Ordering
// }
// These laws must be true:
// Reflexivity: compare(x, x) === true, for every x in A
// Antisymmetry:  if compare(x, y) <= 0 and compare(y, x) <= 0 then x = y, for every x, y in A
// Transitivity: if compare(x, y) === true and compare(y, z) === true,
// then compare(x, z) === true, for every x, y, z in A

import { Ord, ordNumber, contramap, getDualOrd } from "fp-ts/lib/Ord";
import { pipe } from "fp-ts/lib/pipeable";
import { Semigroup, getMeetSemigroup, getJoinSemigroup } from "fp-ts/lib/Semigroup";

describe("given an order we can simply create a generic min function", () => {
  function min<A>(O: Ord<A>): (x: A, y: A) => A {
    return (x, y): A => (O.compare(x, y) === 1 ? y : x);
  }

  describe("use ordNumber to get an order between numbers", () => {
    test("find the min", () => {
      expect(min(ordNumber)(2, 1)).toBe(1);
    });
  });

  describe("use contramap to get an order beetween objects", () => {
    type User = { name: string; age: number };
    const byAge: Ord<User> = pipe(
      ordNumber,
      contramap((user: User) => user.age),
    );
    const getYounger = min(byAge);

    test("it finds the younger user", () => {
      expect(getYounger({ name: "Guido", age: 48 }, { name: "Giulio", age: 45 })).toEqual({
        name: "Giulio",
        age: 45,
      });
    });

    describe("use getDualOrd to get the dual", () => {
      function max<A>(O: Ord<A>): (x: A, y: A) => A {
        return min(getDualOrd(O));
      }
      const getOlder = max(byAge);

      test("it finds the older user", () => {
        expect(getOlder({ name: "Guido", age: 48 }, { name: "Giulio", age: 45 })).toEqual({
          name: "Guido",
          age: 48,
        });
      });
    });
  });
});

describe("if we have an Ord instance we can derive two Semigroup instances", () => {
  /** Takes the minimum of two values */
  const semigroupMin: Semigroup<number> = getMeetSemigroup(ordNumber);
  /** Takes the maximum of two values  */
  const semigroupMax: Semigroup<number> = getJoinSemigroup(ordNumber);

  test("semigroupMin finds the min value", () => {
    expect(semigroupMin.concat(2, 1)).toBe(1);
  });

  test("semigroupMax finds the max value", () => {
    expect(semigroupMax.concat(2, 1)).toBe(2);
  });
});

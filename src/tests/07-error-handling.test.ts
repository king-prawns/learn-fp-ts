import { pipe } from "fp-ts/lib/pipeable";
import { getApplySemigroup, getApplyMonoid, getFirstMonoid, getLastMonoid } from "fp-ts/lib/Option";
import { semigroupSum, Semigroup } from "fp-ts/lib/Semigroup";
import { monoidSum, Monoid, getStructMonoid } from "fp-ts/lib/Monoid";

// In functional programming we work with total (and pure) functions.
// We can get a total function from a partial function f: X ⟶ Y => f': X ⟶ Option(Y)

describe("an algebric data type is a type formed by combining other types", () => {
  describe("when the types are indepenent we use a `product type`", () => {
    describe("example: tuples", () => {
      type Tuple1 = [string]; // I = [0]
      type Tuple2 = [string, number]; // I = [0, 1]
      type Tuple3 = [string, number, boolean]; // I = [0, 1, 2]

      // Accessing by index
      type First = Tuple2[0]; // string
      type Second = Tuple2[1]; // number
    });
    describe("example: tuples", () => {
      type Hour = 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12;
      type Period = "AM" | "PM";
      type Clock = [Hour, Period];
    });
    describe("example: struct", () => {
      // I = {"name", "age"}
      interface Person {
        name: string;
        age: number;
      }
      // Accessing by label
      type Name = Person["name"]; // string
      type Age = Person["age"]; // number
    });
  });

  describe("when the types are depenent we use a `sum type`", () => {
    describe("error handling with type Option", () => {
      type Option<A> =
        | { _tag: "None" } // represents a failure
        | { _tag: "Some"; value: A }; // represents a success

      // constructors
      // (a nullary constructor can be implemented as a constant)
      const none: Option<never> = { _tag: "None" };
      const some = <A>(value: A): Option<A> => ({ _tag: "Some", value });

      describe("example: handle success and failure", () => {
        const fold = <A, R>(onNone: () => R, onSome: (a: A) => R) => (fa: Option<A>): R =>
          fa._tag === "None" ? onNone() : onSome(fa.value);

        function head<A>(as: Array<A>): Option<A> {
          return as.length === 0 ? none : some(as[0]);
        }

        const optionToString: <A>(option: Option<A>) => string = fold(
          () => "Empty array",
          a => String(a),
        );

        test("optionToString can handle a failure", () => {
          expect(pipe(head([]), optionToString)).toBe("Empty array");
        });

        test("optionToString can handle a success", () => {
          expect(pipe(head([1]), optionToString)).toBe("1");
        });
      });

      describe("combining two Options", () => {
        describe("we can get an instance of Semigroup<Option<A>> given a Semigroup<A>", () => {
          const S: Semigroup<Option<number>> = getApplySemigroup(semigroupSum);

          test("it merges two Option<number>, using the sum", () => {
            expect(S.concat(none, none)).toEqual(none);
            expect(S.concat(none, some(1))).toEqual(none);
            expect(S.concat(some(1), none)).toEqual(none);
            expect(S.concat(some(1), some(2))).toEqual(some(3));
          });
        });

        describe("we can get an instance of Monoid<Option<A>> given a Monoid<A>", () => {
          const M: Monoid<Option<number>> = getApplyMonoid(monoidSum);

          test("it merges two Option<number>, using the sum", () => {
            expect(M.concat(none, none)).toEqual(none);
            expect(M.concat(none, some(1))).toEqual(none);
            expect(M.concat(some(1), none)).toEqual(none);
            expect(M.concat(some(1), some(2))).toEqual(some(3));
            expect(M.concat(some(1), M.empty)).toEqual(some(1));
          });
        });

        describe("we can derive two instances of Monoid<Option<A>> for every A", () => {
          const firstMonoid: Monoid<Option<number>> = getFirstMonoid<number>();
          const lastMonoid: Monoid<Option<number>> = getLastMonoid<number>();

          test("firstMonoid returns the first option not empty", () => {
            expect(firstMonoid.concat(none, none)).toEqual(none);
            expect(firstMonoid.concat(none, some(1))).toEqual(some(1));
            expect(firstMonoid.concat(some(1), none)).toEqual(some(1));
            expect(firstMonoid.concat(some(1), some(2))).toEqual(some(1));
          });

          test("lastMonoid returns the last option not empty", () => {
            expect(lastMonoid.concat(none, none)).toEqual(none);
            expect(lastMonoid.concat(none, some(1))).toEqual(some(1));
            expect(lastMonoid.concat(some(1), none)).toEqual(some(1));
            expect(lastMonoid.concat(some(1), some(2))).toEqual(some(2));
          });

          describe("example: handle optional values with lastMonoid", () => {
            interface Settings {
              fontFamily: Option<string>;
              fontSize: Option<number>;
              maxColumn: Option<number>;
            }

            const monoidSettings: Monoid<Settings> = getStructMonoid({
              fontFamily: getLastMonoid<string>(),
              fontSize: getLastMonoid<number>(),
              maxColumn: getLastMonoid<number>(),
            });

            const workspaceSettings: Settings = {
              fontFamily: some("Courier"),
              fontSize: none,
              maxColumn: some(80),
            };

            const userSettings: Settings = {
              fontFamily: some("Fira Code"),
              fontSize: some(12),
              maxColumn: none,
            };

            test("userSettings overrides workspaceSettings", () => {
              expect(monoidSettings.concat(workspaceSettings, userSettings)).toEqual({
                fontFamily: some("Fira Code"),
                fontSize: some(12),
                maxColumn: some(80),
              });
            });
          });
        });
      });
    });

    describe("error handling with type Either", () => {
      type Either<E, A> =
        | { _tag: "Left"; left: E } // represents a failure
        | { _tag: "Right"; right: A }; // represents a success

      const left = <E, A>(left: E): Either<E, A> => ({ _tag: "Left", left });
      const right = <E, A>(right: A): Either<E, A> => ({ _tag: "Right", right });

      describe("example: handle success and failure", () => {
        const fold = <E, A, R>(onLeft: (left: E) => R, onRight: (right: A) => R) => (
          fa: Either<E, A>,
        ): R => (fa._tag === "Left" ? onLeft(fa.left) : onRight(fa.right));

        function head<A>(as: Array<A>): Either<Error, A> {
          return as.length === 0 ? left(new Error("Empty array")) : right(as[0]);
        }

        const eitherToString: <A>(option: Either<Error, A>) => string = fold(
          error => error.message,
          a => String(a),
        );

        test("eitherToString handles a failure", () => {
          expect(pipe(head([]), eitherToString)).toBe("Empty array");
        });

        test("eitherToString handles a success", () => {
          expect(pipe(head([1]), eitherToString)).toBe("1");
        });
      });
    });
  });
});

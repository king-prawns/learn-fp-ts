"use strict";
exports.__esModule = true;
const t = require("io-ts");
const Either_1 = require("fp-ts/lib/Either");
const pipeable_1 = require("fp-ts/lib/pipeable");
const User = t.type({
  key1: t.string,
  key2: t.number,
  key3: t.boolean,
});
const userMockOK = {
  key1: "test_key_1",
  key2: 123,
  key3: false,
};
const userMockKO = {
  key1: "test_key_1",
  key2: "123",
  key3: false,
};
const onLeft = function(error) {
  return error;
};
const onRight = function(user) {
  return user;
};
pipeable_1.pipe(User.decode(userMockKO), Either_1.fold(onLeft, onRight), console.log);
// SAME WITHOUT FP-TS
const result = User.decode(userMockOK);
if (result._tag === "Right") {
  console.log(result.right);
} else {
  throw new Error("something wrong");
}
